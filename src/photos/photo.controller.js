import express from "express";
import path from "path";
import process from "process";
import { fetchAnImage, fetchImages } from "./photo.service.js";
const router = express.Router();

router.get("/", (req, res) => {
  const { limit = 10, page = 0 } = req.query;

  const images = fetchImages(limit, page);

  res.json(images);
});

export default router;
