import images from "./photo.data.js";

export function fetchAnImage(requestedImageId) {
  const image = images.find(({ id }) => id === requestedImageId);
  return image;
}

export function fetchImages(limit, page) {
  const results = [];
  const offset = limit * page;
  let hasMore = false;

  for (const [position, image] of images.entries()) {
    if (position < offset) continue;

    if (results.length < limit) {
      results.push(image);
      continue;
    } else {
      hasMore = true;
      break;
    }
  }

  return {
    results,
    hasMore,
  };
}
