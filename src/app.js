import express from "express";
import compression from "compression";
import { dirname } from "path";
import { fileURLToPath } from "url";
import photosController from "./photos/photo.controller.js";
import cors from "cors";

const app = express();
const __dirname = dirname(fileURLToPath(import.meta.url));

app.set("host", process.env.HOST || "0.0.0.0");
app.set("port", process.env.PORT || 8080);

app.use(cors());
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.disable("x-powered-by");

app.use("/api/photos", photosController);

app.use(express.static(`${__dirname}\\public`));

export default app;
